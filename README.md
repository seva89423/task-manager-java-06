PROJECT INFO
=====================
TASK MANAGER

DEVELOPER INFO
=====================
**NAME:** Vsevolod Zorin

**E-MAIL:** <seva89423@gmail.com>

STACK OF TECHNOLOGIES
=====================
- Java Development Kit v.1.8
- Maven
- Gradle
- IntelliJ IDEA 2020 1.3
- Git Bash 

SOFTWARE
=====================
- JDK 1.8
- MS WINDOWS 10

PROGRAM BUILD
=====================
```
mvn clean install
```

PROGRAM RUN
=====================
```
java -jar ./task-manager.jar
```
