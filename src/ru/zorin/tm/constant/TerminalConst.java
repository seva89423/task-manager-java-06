package ru.zorin.tm.constant;

public interface TerminalConst {

    public static final String HELP = "help";

    public static final String ABOUT = "about";

    public static final String VERSION = "version";

}
