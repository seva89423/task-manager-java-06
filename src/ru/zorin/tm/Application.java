package ru.zorin.tm;

import ru.zorin.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        System.out.println("** WELCOME TO TASK MANAGER **");
        parseArgs(args);
    }

    private static void parseArgs(final String[] args) {
        if (args == null || args.length == 0) return;

        for (String arg : args) {
            chooseCommand(arg);
        }
    }

    private static void chooseCommand(String arg) {
        switch (arg) {
            case TerminalConst.ABOUT:
                showAbout();
                break;
            case TerminalConst.VERSION:
                showVersion();
                break;
            case TerminalConst.HELP:
                showHelp();
                break;
            default:
                System.out.println("Invalid command. Try again.");
        }
    }
	
    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.println(TerminalConst.HELP + " - Show developer info");
        System.out.println(TerminalConst.VERSION + " - Show version info");
        System.out.println(TerminalConst.ABOUT + " - Show display commands");
    }

    public static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Developer name: Vsevolod Zorin");
        System.out.println("E-mail: seva89423@gmail.com");
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.1");
    }

}